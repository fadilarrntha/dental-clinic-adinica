/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author User
 */
public class new_patients {
    private int ptid, ptage;
    private String ptname, pressid;
    
    public new_patients(int ptid, String ptname, int ptage, String pressid) {
        this.ptid = ptid;
        this.ptage = ptage;
        this.ptname = ptname;
        this.pressid = pressid;
    }
    public int getid(){
        return ptid;
    }
    public String getname(){
        return ptname;
    }
    public int getage(){
        return ptage;
    }
    public String pressid(){
        return pressid;
    }
}
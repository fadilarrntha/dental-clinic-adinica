/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;
import java.sql.*;
import Config.Connect;
import Config.Connect;
import java.util.ArrayList;
import javax.swing.*;
import View.Invoice;
import Model.new_patients;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class NewPatients extends javax.swing.JFrame {
    /**
     * Creates new form ClinicalWork
     */
    Connection conn = null;
    PreparedStatement ps = null;
    ResultSet rs = null;
    public NewPatients() {
        initComponents();
        conn = Connect.connectDb();
        showNewPatients();
    }
    
    public ArrayList<new_patients> patientsList() {
        ArrayList<new_patients> patientsList = new ArrayList<new_patients>();
        try {
            String qry1 = "SELECT * FROM patients WHERE pt_status = 'New'";
            ps = conn.prepareStatement(qry1);
            ResultSet rs = ps.executeQuery();
            new_patients data_list;
            while(rs.next()) {
                data_list = new new_patients(rs.getInt(1), rs.getString(2), rs.getInt(4), rs.getString(7));
                patientsList.add(data_list);
            }
        }catch(Exception e){
            JOptionPane.showMessageDialog(null, e);
        }
        return patientsList;
    }
    
    private void showNewPatients() {
        ArrayList<new_patients> list = patientsList();
        DefaultTableModel model = (DefaultTableModel)NewPatientTable.getModel();
        Object[] row = new Object[5];        
        for (int i = 0; i < list.size(); i++) {
            JButton button = new JButton();
            row[0] = list.get(i).getid();
            row[1] = list.get(i).getage();
            row[2] = list.get(i).getname();
            row[3] = list.get(i).pressid();
            row[4] = "Cost Here...";//+list.get(i).getid();
            NewPatientTable.setRowHeight(30);
            model.addRow(row);
        }
        NewPatientTable.addMouseListener(new java.awt.event.MouseAdapter() {
        @Override
            public void mouseClicked(java.awt.event.MouseEvent evt) {
            int row = NewPatientTable.rowAtPoint(evt.getPoint());
            String patient_details[] = new String[3];
            patient_details[0] = list.get(row).getname();
            patient_details[1] = Integer.toString(list.get(row).getage());
            patient_details[2] = list.get(row).pressid();
            Invoice patientPrescription = new Invoice(patient_details);
            patientPrescription.setVisible(true);
        }
    });
}
    

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        NewPatientTable = new javax.swing.JTable();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        Invoice = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setTitle("Record of all new patients");
        setBackground(new java.awt.Color(255, 255, 255));
        setMaximizedBounds(new java.awt.Rectangle(0, 0, 1365, 735));
        setResizable(false);
        setSize(new java.awt.Dimension(1365, 735));
        setType(java.awt.Window.Type.POPUP);

        jPanel1.setBackground(new java.awt.Color(167, 206, 219));

        jLabel1.setFont(new java.awt.Font("Times New Roman", 0, 28)); // NOI18N
        jLabel1.setText("New Patient's Record");

        NewPatientTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Patient's ID", "Patient's Name", "Patient's Age", "Diagnosis", "Fee"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        NewPatientTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                NewPatientTableMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(NewPatientTable);
        if (NewPatientTable.getColumnModel().getColumnCount() > 0) {
            NewPatientTable.getColumnModel().getColumn(0).setResizable(false);
            NewPatientTable.getColumnModel().getColumn(1).setResizable(false);
            NewPatientTable.getColumnModel().getColumn(2).setResizable(false);
            NewPatientTable.getColumnModel().getColumn(3).setResizable(false);
        }

        jButton1.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        jButton1.setText("Close Window");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton2.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        jButton2.setText("Done");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        Invoice.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        Invoice.setText("Invoice");
        Invoice.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                InvoiceActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 1263, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(Invoice)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton1)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 496, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton1)
                    .addComponent(jButton2)
                    .addComponent(Invoice))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        this.dispose();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void NewPatientTableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_NewPatientTableMouseClicked
        // TODO add your handling code here:
//        System.out.println("babi");
//                int row = NewPatientTable.getSelectedRow();
//                String id = NewPatientTable.getValueAt(row, 0).toString();
//                System.out.println(id);
//        String id = (String) NewPatientTable.getValueAt(row,1);
//        System.out.println(id);
    }//GEN-LAST:event_NewPatientTableMouseClicked

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
        int row = NewPatientTable.getSelectedRow();
        int id = Integer.parseInt( NewPatientTable.getValueAt(row,0).toString());
        
        try{
            String sql ="UPDATE patients SET pt_status ='Old'"
                    +" WHERE pt_id ="+id;
            java.sql.Connection conn=(Connection)Connect.connectDb();
            java.sql.PreparedStatement pstm=conn.prepareStatement(sql);
            pstm.execute();
            JOptionPane.showMessageDialog(null, "Data berhasil di edit");
            DefaultTableModel model = (DefaultTableModel)NewPatientTable.getModel();
            model.setRowCount(0);
            showNewPatients();
        } catch (Exception e){
            JOptionPane.showMessageDialog(this,e.getMessage());
        }
    }//GEN-LAST:event_jButton2ActionPerformed

    private void InvoiceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_InvoiceActionPerformed
        // TODO add your handling code here:
//        NewPatientTable.addMouseListener(new java.awt.event.MouseAdapter() {
//        @Override
//            public void mouseClicked(java.awt.event.MouseEvent evt) {
//            int row = NewPatientTable.rowAtPoint(evt.getPoint());
//            String patient_details[] = new String[3];
//            Invoice patientPrescription = new Invoice(patient_details);
//            patientPrescription.setVisible(true);
//        }
//    });
    }//GEN-LAST:event_InvoiceActionPerformed

    /**
     * @param args the command line arguments
     */
//    public static void main(String args[]) {
//        /* Set the Nimbus look and feel */
//        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
//        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
//         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
//         */
//        try {
//            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
//                if ("Nimbus".equals(info.getName())) {
//                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
//                    break;
//                }
//            }
//        } catch (ClassNotFoundException ex) {
//            java.util.logging.Logger.getLogger(ClinicalWork.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (InstantiationException ex) {
//            java.util.logging.Logger.getLogger(ClinicalWork.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (IllegalAccessException ex) {
//            java.util.logging.Logger.getLogger(ClinicalWork.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
//            java.util.logging.Logger.getLogger(ClinicalWork.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        }
//        //</editor-fold>
//
//        /* Create and display the form */
//        java.awt.EventQueue.invokeLater(new Runnable() {
//            public void run() {
//                //new ClinicalWork().setVisible(true);
//            }
//        });
//    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton Invoice;
    private javax.swing.JTable NewPatientTable;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    // End of variables declaration//GEN-END:variables

}
